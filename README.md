# T-AIA-800
===========
This repository contain dataset for my_ocr (NumberNet) and EagleEyes project's.

## my_ocr
MNIST dataset
http://yann.lecun.com/exdb/mnist/


## EgleEyes
Norb Dataset
https://cs.nyu.edu/~ylclab/data/norb-v1.0/ 
